# Spring Boot Stripe Integration
This project is for testing with Stripe API for Billing Service used by Employer Center.

### Testing Steps
- Create a customer, get customer ID. Use `io.c12.bala.stripe.runner.CreateCustomer.java`
- Create a Payment Method (Credit card) for the customer. Use `io.c12.bala.stripe.runner.CreatePaymentMethod`
  - First Create a payment method, which is credit card. Need card info, and address
  - Once the payment method is created successfully, attach it to a customer using Customer ID.
- Charge card, need customer ID and Payment method ID from above two calls. Use `io.c12.bala.stripe.runner.CreateCharge`
  - Need to create a Payment Intent first. This creates a unconfirmed charge.
  - Then need to confirm the Payment Intent to confirm the payment.
