package io.c12.bala.stripe.service;

import com.stripe.model.Card;
import com.stripe.model.Customer;
import com.stripe.net.RequestOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(7)
public class StripeAddCardService implements CommandLineRunner {

    RequestOptions requestOptions = RequestOptions.builder()
            .setApiKey("sk_test_51L1G9VBhmn2nU6LTBYtxBsbpdq3eDovayPRmuBPgTkvcKUi5c00PAM8tNtJ5k7ON6I7TSjUrW5Ewu7tZ5p75DvsX00CgISJPzI")
            .build();

    /**
     * TODO: This is not working with api. Have to use payment method as {@code code} instead of card.
     * @param args incoming main method arguments
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        Map<String, Object> retrieveParams = new HashMap<>();
        List<String> expandList = new ArrayList<>();
        expandList.add("sources");
        retrieveParams.put("expand", expandList);
        Customer customer = Customer.retrieve("cus_LjW6HstK58AALP", retrieveParams, requestOptions);

        Map<String, Object> source = new HashMap<>();
        source.put("object", "card");
        source.put("number", "6011000990139424");
        source.put("exp_month", "12");
        source.put("exp_year", "2031");
        source.put("cvc", "872");
        source.put("name", "Gugan Bala");
        source.put("address_line1", "711 Arapahoe Ave");
        source.put("address_city", "Satanta");
        source.put("address_state", "KS");
        source.put("address_zip", "67870");
        source.put("address_country", "US");
        Map<String, Object> params = new HashMap<>();
        params.put("source", source);

        Card card = (Card) customer.getSources().create(params, requestOptions);
        log.info("Card - {}", card);
    }
}
