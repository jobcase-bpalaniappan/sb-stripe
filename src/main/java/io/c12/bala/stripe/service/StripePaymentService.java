package io.c12.bala.stripe.service;

import com.stripe.model.Customer;
import com.stripe.model.PaymentIntent;
import com.stripe.model.PaymentMethod;
import com.stripe.net.RequestOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(1)
public class StripePaymentService implements CommandLineRunner {

    RequestOptions requestOptions = RequestOptions.builder()
            .setApiKey("sk_test_51L1G9VBhmn2nU6LTBYtxBsbpdq3eDovayPRmuBPgTkvcKUi5c00PAM8tNtJ5k7ON6I7TSjUrW5Ewu7tZ5p75DvsX00CgISJPzI")
            .build();

    @Override
    public void run(String... args) throws Exception {
        // Step 1: Create a customer
        Map<String, Object> customerParams = new HashMap<>();
        customerParams.put("name", "ACME Inc");
        customerParams.put("email", "bala@jobcase.com");
        customerParams.put("description", "Test ACME company for Stripe payment testing");
        customerParams.put("phone", "5155556789");

        Customer customer = Customer.create(customerParams, requestOptions);
        log.info("Customer info - {}", customer);

        // Step 2: Create a card
        Map<String, Object> card = new HashMap<>();
        card.put("number", "4242424242424242");
        card.put("exp_month", 5);
        card.put("exp_year", 2023);
        card.put("cvc", "314");
        Map<String, Object> cardParams = new HashMap<>();
        cardParams.put("type", "card");
        cardParams.put("card", card);

        Map<String, Object> billingDetails = new HashMap<>();
        billingDetails.put("email", "bala@jobcase.com");
        billingDetails.put("name", "Bala Palaniappan");
        billingDetails.put("phone", "5155155115");

        Map<String, Object> addressParams = new HashMap<>();
        addressParams.put("line1", "1100 University Ave");
        addressParams.put("city", "Des Moines");
        addressParams.put("state", "IA");
        addressParams.put("postal_code", "50391");
        addressParams.put("country", "US");

        billingDetails.put("address", addressParams);
        cardParams.put("billing_details", billingDetails);

        PaymentMethod paymentMethod = PaymentMethod.create(cardParams, requestOptions);
        log.info("Payment Card - {}", paymentMethod);

        // Step 3: Add a card to Customer
        Map<String, Object> params = new HashMap<>();
        params.put("customer", customer.getId());

        PaymentMethod updatedPaymentMethod = paymentMethod.attach(params, requestOptions);
        log.info("Updated Payment Method - {}", updatedPaymentMethod);

        // Step 4: Create Payment Intend
        List<Object> paymentMethodTypes = new ArrayList<>();
        paymentMethodTypes.add("card");
        Map<String, Object> paymentIntentParams = new HashMap<>();
        paymentIntentParams.put("amount", 4356);
        paymentIntentParams.put("currency", "usd");
        paymentIntentParams.put("description", "Hello World");
        paymentIntentParams.put("payment_method_types", paymentMethodTypes);
        paymentIntentParams.put("payment_method", paymentMethod.getId());
        paymentIntentParams.put("customer", customer.getId());

        PaymentIntent paymentIntent = PaymentIntent.create(paymentIntentParams, requestOptions);
        log.info("Payment Intent - {}", paymentIntent);

        PaymentIntent updatedPaymentIntent = paymentIntent.confirm(requestOptions);
        log.info("Updated payment intent - {}", updatedPaymentIntent);
    }
}
