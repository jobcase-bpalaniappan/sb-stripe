package io.c12.bala.stripe.service;

import com.stripe.model.Charge;
import com.stripe.model.ChargeCollection;
import com.stripe.net.RequestOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(5)
public class StripeListChargesService implements CommandLineRunner {

    RequestOptions requestOptions = RequestOptions.builder()
            .setApiKey("sk_test_51L1G9VBhmn2nU6LTBYtxBsbpdq3eDovayPRmuBPgTkvcKUi5c00PAM8tNtJ5k7ON6I7TSjUrW5Ewu7tZ5p75DvsX00CgISJPzI")
            .build();

    @Override
    public void run(String... args) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("customer", "cus_LjW6HstK58AALP");
        params.put("limit", 40);

        ChargeCollection charges = Charge.list(params, requestOptions);
        log.info("Customer Charges - {}", charges);
    }
}
