package io.c12.bala.stripe.service;

import com.stripe.model.PaymentIntent;
import com.stripe.net.RequestOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(4)
public class StripeMakePaymentService implements CommandLineRunner {

    RequestOptions requestOptions = RequestOptions.builder()
            .setApiKey("sk_test_51L1G9VBhmn2nU6LTBYtxBsbpdq3eDovayPRmuBPgTkvcKUi5c00PAM8tNtJ5k7ON6I7TSjUrW5Ewu7tZ5p75DvsX00CgISJPzI")
            .build();

    @Override
    public void run(String... args) throws Exception {
        List<Object> paymentMethodTypes = new ArrayList<>();
        paymentMethodTypes.add("card");
        Map<String, Object> paymentIntentParams = new HashMap<>();
        paymentIntentParams.put("amount", 9900);
        paymentIntentParams.put("currency", "usd");
        paymentIntentParams.put("description", "Payment Three | Job # 9q8u9qud2093u");
        paymentIntentParams.put("payment_method_types", paymentMethodTypes);
        paymentIntentParams.put("payment_method", "pm_1L234MBhmn2nU6LTGI7J2ZPr");
        paymentIntentParams.put("customer", "cus_LjW6HstK58AALP");

        PaymentIntent paymentIntent = PaymentIntent.create(paymentIntentParams, requestOptions);
        log.info("Payment Intent - {}", paymentIntent);

        PaymentIntent updatedPaymentIntent = paymentIntent.confirm(requestOptions);
        log.info("Updated payment intent - {}", updatedPaymentIntent);
    }
}
