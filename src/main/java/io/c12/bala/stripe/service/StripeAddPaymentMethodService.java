package io.c12.bala.stripe.service;

import com.stripe.model.Customer;
import com.stripe.model.PaymentMethod;
import com.stripe.net.RequestOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(2)
public class StripeAddPaymentMethodService implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        // Step 1: Create a customer
        Map<String, Object> customerParams = new HashMap<>();
        customerParams.put("name", "Cloud Inc");
        customerParams.put("email", "bala@c12.io");
        customerParams.put("description", "Test Cloud company for Stripe payment testing");
        customerParams.put("phone", "5155552836");

        Customer customer = Customer.create(customerParams, requestOptions);
        log.info("Customer info - {}", customer);

        // Step 2: Add payment methods.
        Map<String, Object> card = new HashMap<>();
        card.put("number", "4242424242424242");
        card.put("exp_month", 5);
        card.put("exp_year", 2023);
        card.put("cvc", "314");
        Map<String, Object> cardParams = new HashMap<>();
        cardParams.put("type", "card");
        cardParams.put("card", card);

        Map<String, Object> billingDetails = new HashMap<>();
        billingDetails.put("email", "bala@c12.io");
        billingDetails.put("name", "Bala Palaniappan");
        billingDetails.put("phone", "5155155115");

        Map<String, Object> addressParams = new HashMap<>();
        addressParams.put("line1", "1100 University Ave");
        addressParams.put("city", "Des Moines");
        addressParams.put("state", "IA");
        addressParams.put("postal_code", "50391");
        addressParams.put("country", "US");

        billingDetails.put("address", addressParams);
        cardParams.put("billing_details", billingDetails);

        PaymentMethod paymentMethod = PaymentMethod.create(cardParams, requestOptions);
        log.info("Payment Card - {}", paymentMethod);

        // Step 3: Add a card to Customer
        Map<String, Object> params = new HashMap<>();
        params.put("customer", customer.getId());

        PaymentMethod updatedPaymentMethod = paymentMethod.attach(params, requestOptions);
        log.info("Updated Payment Method - {}", updatedPaymentMethod);

        // Step 2: Add payment methods.
        card = new HashMap<>();
        card.put("number", "5555555555554444");
        card.put("exp_month", 12);
        card.put("exp_year", 2024);
        card.put("cvc", "675");
        cardParams = new HashMap<>();
        cardParams.put("type", "card");
        cardParams.put("card", card);

        billingDetails = new HashMap<>();
        billingDetails.put("email", "balap@c12.io");
        billingDetails.put("name", "John Doe");
        billingDetails.put("phone", "5155155468");

        addressParams = new HashMap<>();
        addressParams.put("line1", "402 Macgregor Rd");
        addressParams.put("city", "Winter Springs");
        addressParams.put("state", "FL");
        addressParams.put("postal_code", "32708");
        addressParams.put("country", "US");

        billingDetails.put("address", addressParams);
        cardParams.put("billing_details", billingDetails);

        paymentMethod = PaymentMethod.create(cardParams, requestOptions);
        log.info("Payment Card - {}", paymentMethod);

        // Step 3: Connect Payment Method to Customer
        params = new HashMap<>();
        params.put("customer", customer.getId());

        updatedPaymentMethod = paymentMethod.attach(params, requestOptions);
        log.info("Updated Payment Method - {}", updatedPaymentMethod);

        // Step 2: Add payment methods.
        card = new HashMap<>();
        card.put("number", "378282246310005");
        card.put("exp_month", 4);
        card.put("exp_year", 2025);
        card.put("cvc", "1178");
        cardParams = new HashMap<>();
        cardParams.put("type", "card");
        cardParams.put("card", card);

        billingDetails = new HashMap<>();
        billingDetails.put("email", "balap1@c12.io");
        billingDetails.put("name", "Jack Reacher");
        billingDetails.put("phone", "5155158678");

        addressParams = new HashMap<>();
        addressParams.put("line1", "306 N Maple St");
        addressParams.put("city", "Jefferson");
        addressParams.put("state", "IA");
        addressParams.put("postal_code", "50129");
        addressParams.put("country", "US");

        billingDetails.put("address", addressParams);
        cardParams.put("billing_details", billingDetails);

        paymentMethod = PaymentMethod.create(cardParams, requestOptions);
        log.info("Payment Card - {}", paymentMethod);

        // Step 3: Connect Payment Method to Customer
        params = new HashMap<>();
        params.put("customer", customer.getId());

        updatedPaymentMethod = paymentMethod.attach(params, requestOptions);
        log.info("Updated Payment Method - {}", updatedPaymentMethod);

    }
}
