package io.c12.bala.stripe.service;

import com.stripe.model.PaymentMethod;
import com.stripe.net.RequestOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(6)
public class StripeRemoveCardService implements CommandLineRunner {
    RequestOptions requestOptions = RequestOptions.builder()
            .setApiKey("sk_test_51L1G9VBhmn2nU6LTBYtxBsbpdq3eDovayPRmuBPgTkvcKUi5c00PAM8tNtJ5k7ON6I7TSjUrW5Ewu7tZ5p75DvsX00CgISJPzI")
            .build();

    @Override
    public void run(String... args) throws Exception {
        // To remove or delete, it has to be detaching Payment Method from Customer.
        PaymentMethod paymentMethod = PaymentMethod.retrieve("pm_1L234OBhmn2nU6LTLTYhRLfS", requestOptions);

        PaymentMethod updatedPaymentMethod = paymentMethod.detach(requestOptions);
        log.info("Detached payment method - {}", updatedPaymentMethod);
    }
}
