package io.c12.bala.stripe.runner;

import com.stripe.model.Charge;
import com.stripe.model.ChargeSearchResult;
import com.stripe.param.ChargeSearchParams;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(22)
public class SearchPaymentCharges implements CommandLineRunner {

  public static final String CUSTOMER_ID = "cus_Nf6ekfjoyxVs3L";

  /**
   * Search for Payment charges based on metadata search.
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure.
   */
  @Override
  public void run(String... args) throws Exception {
    ChargeSearchParams params = ChargeSearchParams.builder()
      .setQuery("customer:'cus_Nf6ekfjoyxVs3L' AND metadata['sub_organization_id']:'19289a68-945c-4206-9b79-096c6e3cd574'")
      .setLimit(100L)
      .build();
    ChargeSearchResult result = Charge.search(params, requestOptions);

    log.info("Customer Charges --> \n {}", result);
  }
}
