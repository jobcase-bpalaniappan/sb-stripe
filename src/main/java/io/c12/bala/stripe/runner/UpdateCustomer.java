package io.c12.bala.stripe.runner;

import com.stripe.model.Customer;
import com.stripe.param.CustomerUpdateParams;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.datafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(12)
public class UpdateCustomer implements CommandLineRunner {

  public static final String CUSTOMER_ID = "cus_MDkBfcDOj17F8e";

  @Override
  public void run(String... args) throws Exception {
    Faker faker = new Faker();

    // retrieve customer entity from stripe
    Customer customer = Customer.retrieve(CUSTOMER_ID, requestOptions);
    log.info("Retrieved Customer - {}", customer);

    final CustomerUpdateParams customerUpdateParams = CustomerUpdateParams.builder().setName(faker.company().name()).build();
    Customer updatedCustomer = customer.update(customerUpdateParams, requestOptions);
    log.info("Updated customer - {}", updatedCustomer);
  }
}
