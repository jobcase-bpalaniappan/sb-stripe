package io.c12.bala.stripe.runner;

import com.stripe.model.Card;
import com.stripe.model.Customer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(17)
public class CreateCard implements CommandLineRunner {

  private static final String CUSTOMER_ID = "cus_LjY1KnfAQ0kdZL";
  private static final String CREDIT_CARD_SOURCE = "tok_mastercard";

  @Override
  public void run(String... args) throws Exception {
    Map<String, Object> retrieveParams = new HashMap<>();
    List<String> expandList = new ArrayList<>();
    expandList.add("sources");
    retrieveParams.put("expand", expandList);
    Customer customer = Customer.retrieve(CUSTOMER_ID, retrieveParams, requestOptions);

    Map<String, Object> params = new HashMap<>();
    params.put("source", CREDIT_CARD_SOURCE);

    Card card = (Card) customer.getSources().create(params, requestOptions);
    log.info("Credit card added - {}", card);
  }
}
