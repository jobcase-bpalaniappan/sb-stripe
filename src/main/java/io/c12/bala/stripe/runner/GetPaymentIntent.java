package io.c12.bala.stripe.runner;

import com.stripe.model.PaymentIntent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(21)
public class GetPaymentIntent implements CommandLineRunner {

  private static final String PAYMENT_INTENT_ID = "pi_3N5uHKBhmn2nU6LT0mBYTSkY";

  /**
   * Get Payment Intent by Payment Intent ID.
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure.
   */
  @Override
  public void run(String... args) throws Exception {
    PaymentIntent paymentIntent = PaymentIntent.retrieve(PAYMENT_INTENT_ID, requestOptions);
    log.info("Payment Intent --> \n {}", paymentIntent);
  }
}
