package io.c12.bala.stripe.runner;

import com.stripe.model.Charge;
import com.stripe.model.ChargeCollection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(14)
public class ListPaymentCharges implements CommandLineRunner {

  public static final String CUSTOMER_ID = "cus_NrdI2k7e5FmpBb";

  /**
   * List all Payment Charges made for a customer. Including all payment methods.
   * This get all the Card Transactions for a customer
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure.
   */
  @Override
  public void run(String... args) throws Exception {
    Map<String, Object> params = new HashMap<>();
    params.put("customer", CUSTOMER_ID);
    params.put("limit", 40);

    ChargeCollection charges = Charge.list(params, requestOptions);
    log.info("Customer Charges --> \n {}", charges);
  }
}
