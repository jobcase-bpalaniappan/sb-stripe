package io.c12.bala.stripe.runner;

import com.stripe.model.PaymentMethod;
import com.stripe.model.PaymentMethodCollection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(13)
public class ListPaymentMethods implements CommandLineRunner {

  public static final String CUSTOMER_ID = "cus_NrdI2k7e5FmpBb";

  /**
   * List all Payment Methods for a customer by Customer ID.
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure.
   */
  @Override
  public void run(String... args) throws Exception {
    Map<String, Object> params = new HashMap<>();
    params.put("customer", CUSTOMER_ID);
    params.put("type", "card");

    PaymentMethodCollection paymentMethods = PaymentMethod.list(params, requestOptions);
    log.info("List all cards --> \n {}", paymentMethods);
  }
}
