package io.c12.bala.stripe.runner;

import com.stripe.model.PaymentMethod;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(16)
public class GetPaymentMethod implements CommandLineRunner {

  private static final String PAYMENT_METHOD_ID = "pm_1N5u6ZBhmn2nU6LT1u6WDib9";

  private static final String CUSTOMER_ID = null;

  /**
   * Get existing Payment Method (credit card) by Payment Method ID.
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure.
   */
  @Override
  public void run(String... args) throws Exception {
    PaymentMethod paymentMethod = PaymentMethod.retrieve(PAYMENT_METHOD_ID, requestOptions);
    log.info("Payment Method --> \n {}", paymentMethod);

    // Attach Payment Method to Customer if Customer ID is not null
    if (CUSTOMER_ID != null) {
      Map<String, Object> params = new HashMap<>();
      params.put("customer", CUSTOMER_ID);

      PaymentMethod updatedPaymentMethod = paymentMethod.attach(params, requestOptions);
      log.info("Updated Payment Method --> \n {}", updatedPaymentMethod);
    }
  }
}
