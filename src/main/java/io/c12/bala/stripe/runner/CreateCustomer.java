package io.c12.bala.stripe.runner;

import com.stripe.model.Customer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.datafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(10)
public class CreateCustomer implements CommandLineRunner {

  /**
   * Create a customer with Basic info in Stripe. This returns a customer id.
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure
   */
  @Override
  public void run(String... args) throws Exception {
    Faker faker = new Faker();
    // Customer info
    Map<String, Object> customerParams = new HashMap<>();
    customerParams.put("name", faker.company().name());
    customerParams.put("email", faker.internet().emailAddress());
    customerParams.put("description", "Test company for Stripe payment testing");
    customerParams.put("phone", faker.phoneNumber().phoneNumber());

    // Metadata info
    Map<String, Object> metadataParams = new HashMap<>();
    metadataParams.put("organizationId", UUID.randomUUID().toString());
    metadataParams.put("customerId", UUID.randomUUID().toString());
    customerParams.put("metadata", metadataParams);

    // Create customer
    Customer customer = Customer.create(customerParams, requestOptions);
    log.info("Customer info --> \n{}", customer);
    log.info(" ---- Customer id -- {}", customer.getId());
  }
}
