package io.c12.bala.stripe.runner;

import com.stripe.model.PaymentIntent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(20)
public class ListPaymentIntent implements CommandLineRunner {

  public static final String CUSTOMER_ID = "cus_MGMjXto8XfuWeH";

  @Override
  public void run(String... args) throws Exception {
    Map<String, Object> params = new HashMap<>();
    params.put("customer", CUSTOMER_ID);
    params.put("limit", 40);

    var paymentIntentList = PaymentIntent.list(params, requestOptions);
    log.info("Payment Intent list - {}", paymentIntentList);
  }
}
