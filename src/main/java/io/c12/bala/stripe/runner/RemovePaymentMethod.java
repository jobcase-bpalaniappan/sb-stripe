package io.c12.bala.stripe.runner;

import com.stripe.model.PaymentMethod;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

/**
 * To Remove or Delete Payment Method (Credit card).
 * Detach Payment Method from customer to remove the card.
 */
@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(15)
public class RemovePaymentMethod implements CommandLineRunner {

  public static final String PAYMENT_METHOD_ID = "pm_1L25JUBhmn2nU6LTKJXMazFs";

  @Override
  public void run(String... args) throws Exception {
    // Retrieve Payment Method with payment_method_id.
    PaymentMethod paymentMethod = PaymentMethod.retrieve(PAYMENT_METHOD_ID, requestOptions);

    // Detach Payment Method from customer.
    PaymentMethod updatedPaymentMethod = paymentMethod.detach(requestOptions);
    log.info("Detached payment method - {}", updatedPaymentMethod);
  }
}
