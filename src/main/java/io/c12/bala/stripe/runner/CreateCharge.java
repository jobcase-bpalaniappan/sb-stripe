package io.c12.bala.stripe.runner;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.stripe.exception.CardException;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(12)
public class CreateCharge implements CommandLineRunner {

  public static final String CUSTOMER_ID = "cus_NrdI2k7e5FmpBb";
  public static final String PAYMENT_METHOD_ID = "pm_1N5u6ZBhmn2nU6LT1u6WDib9";

  @Override
  public void run(String... args) throws Exception {
    Random random = new Random();

    List<Object> paymentMethodTypes = new ArrayList<>();
    paymentMethodTypes.add("card");
    Map<String, Object> paymentIntentParams = new HashMap<>();
    paymentIntentParams.put("amount", random.nextInt(10000));
    paymentIntentParams.put("currency", "usd");
    paymentIntentParams.put("description", "Payment one | Job # " + NanoIdUtils.randomNanoId());
    paymentIntentParams.put("payment_method_types", paymentMethodTypes);
    paymentIntentParams.put("payment_method", PAYMENT_METHOD_ID);
    paymentIntentParams.put("customer", CUSTOMER_ID);

    try {
      PaymentIntent paymentIntent = PaymentIntent.create(paymentIntentParams, requestOptions);
      log.info("Payment Intent --> \n {}", paymentIntent);

      PaymentIntent updatedPaymentIntent = paymentIntent.confirm(requestOptions);
      log.info("Updated payment intent --> \n {}", updatedPaymentIntent);
    } catch (CardException ce) {
      log.error("Error charging card... {} | {} | {} | {} |", ce.getCode(), ce.getCharge(), ce.getDeclineCode(), ce.getMessage(), ce);
    } catch (StripeException se) {
      log.error("Stripe Exception -- {} || {} || {} ||", se.getCode(), se.getStripeError().getDeclineCode(), se.getStripeError().getMessage(), se);
    }
  }
}
