package io.c12.bala.stripe.runner;

import com.stripe.model.Charge;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(18)
public class GetPaymentCharge implements CommandLineRunner {

  public static final String PAYMENT_CHARGE_ID = "ch_3N5uHKBhmn2nU6LT0XRKsLyq";

  /**
   * Get existing Credit Card Charge by Charge ID.
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure.
   */
  @Override
  public void run(String... args) throws Exception {
    Charge charge = Charge.retrieve(PAYMENT_CHARGE_ID, requestOptions);
    log.info("Payment Method --> \n {}", charge);
  }
}
