package io.c12.bala.stripe.runner;

import com.stripe.model.PaymentMethod;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.datafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static io.c12.bala.stripe.config.StripeCredentialConfig.requestOptions;

@Profile("test")
@Service
@Slf4j
@RequiredArgsConstructor
@Order(11)
public class CreatePaymentMethod implements CommandLineRunner {

  public static final String CUSTOMER_ID = "cus_NrdI2k7e5FmpBb";

  /**
   * Add a payment method to existing customer. In this case the payment method is going to be Credit Card.
   * First create a Payment Card with Credit card info and Billing info + Address. And then attach the card to existing customer ID.
   *
   * @param args incoming main method arguments
   * @throws Exception on any failure.
   */
  @Override
  public void run(String... args) throws Exception {
    Faker faker = new Faker();

    // Card info
    Map<String, Object> card = new HashMap<>();
    card.put("number", "3056930009020004");
    card.put("exp_month", 2);
    card.put("exp_year", 2026);
    card.put("cvc", "093");

    Map<String, Object> cardParams = new HashMap<>();
    cardParams.put("type", "card");
    cardParams.put("card", card);

    // card billing details
    Map<String, Object> billingDetails = new HashMap<>();
    billingDetails.put("email", faker.internet().emailAddress());
    billingDetails.put("name", faker.name().fullName());
    billingDetails.put("phone", faker.phoneNumber().subscriberNumber());

    // card billing address
    Map<String, Object> addressParams = new HashMap<>();
    addressParams.put("line1", faker.address().streetAddress());
    addressParams.put("city", faker.address().city());
    addressParams.put("state", faker.address().stateAbbr());
    addressParams.put("postal_code", faker.address().zipCode());
    addressParams.put("country", "US");

    billingDetails.put("address", addressParams);
    cardParams.put("billing_details", billingDetails);

    // Payment Method
    PaymentMethod paymentMethod = PaymentMethod.create(cardParams, requestOptions);
    log.info("Payment Card --> \n {}", paymentMethod);

    // only attach the payment method to customer when customer id is available
    if (CUSTOMER_ID != null) {
      // Connect Payment Method to Customer
      Map<String, Object> params = new HashMap<>();
      params.put("customer", CUSTOMER_ID);

      PaymentMethod updatedPaymentMethod = paymentMethod.attach(params, requestOptions);
      log.info("Updated Payment Method --> \n {}", updatedPaymentMethod);
    }
  }
}
